package com.happypeople.ticketstack;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import io.reactiverse.pgclient.PgClient;
import io.reactiverse.pgclient.PgPool;
import io.reactiverse.pgclient.PgPoolOptions;
import io.reactiverse.pgclient.PgRowSet;
import io.reactiverse.pgclient.Row;
import io.reactiverse.pgclient.Tuple;
import io.vertx.core.AsyncResult;

/**
 * For reactive-pg-client see https://github.com/reactiverse/reactive-pg-client
 */
public class TicketEntryListImpl implements TicketEntryList {

	private PgPool pgPool;

	/**
	 * Quick 'n dirty configuration, should be injected
	 * 
	 * @return The reactive database connection factory, like a jdbc driver
	 */
	public static PgPool init() {
		PgPoolOptions options = new PgPoolOptions().setPort(5432).setHost("postgres").setDatabase("ticketstack")
				.setUser("postgres").setPassword("postgres").setMaxSize(3);

		// Create the client pool
		return PgClient.pool(options);
	}

	TicketEntryListImpl(final PgPool pgPool) {
		this.pgPool = pgPool;
	}

	@Override
	public List<TicketEntry> getAllTicketEntries() {
		final CountDownLatch latch = new CountDownLatch(1); // block and return result
		final Throwable[] err = { null };
		final List<TicketEntry> ret = new ArrayList<TicketEntry>();

		pgPool.query("SELECT * FROM tickets ORDER BY prio", handle -> {
			try {
				if (handle.succeeded()) {
					handle.result().forEach(row -> {
						ret.add(rowmap(row));
					});
				} else
					err[0] = handle.cause();
			} finally {
				latch.countDown();
			}
		});

		await(latch); // since this method implements a blocking api we have to wait
		if (err[0] != null)
			throw new RuntimeException("error", err[0]);
		return ret;
	}

	@Override
	public TicketEntry getTicketEntry(String id) {
		final CountDownLatch latch = new CountDownLatch(1); // block and return result
		final Throwable[] err = { null };
		final TicketEntry[] ret = { null };
		pgPool.preparedQuery("SELECT * FROM tickets WHERE ticket=$1", Tuple.of(id), handle -> {
			try {
				if (handle.succeeded()) {
					handle.result().forEach(row -> {
						ret[0]=rowmap(row);
					});
				} else
					err[0] = handle.cause();
			} finally {
				latch.countDown();
			}
		});

		await(latch); // since this method implements a blocking api we have to wait
		if (err[0] != null)
			throw new RuntimeException("error", err[0]);
		return ret[0];
	}

	private void await(CountDownLatch latch) {
		try {
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateTicketText(String id, String updText) {
		final CountDownLatch latch = new CountDownLatch(1); // block and return result
		final Throwable[] err = { null };

		pgPool.preparedQuery("UPDATE tickets SET text=$1 WHERE ticket=$2", Tuple.of(updText, id), handle -> {
			try {
				if (!handle.succeeded())
					err[0] = handle.cause();
			} finally {
				latch.countDown();
			}
		});

		await(latch); // since this method implements a blocking api we have to wait
		if (err[0] != null)
			throw new RuntimeException("error", err[0]);
	}

	@Override
	public void insertTicket(TicketEntry ticketEntry) {
		final CountDownLatch latch = new CountDownLatch(1); // block and return result
		final Throwable[] err = { null };

		pgPool.preparedQuery("INSERT INTO tickets (ticket, text, prio) VALUES($1, $2, $3) ",
				Tuple.of(ticketEntry.getTicket(), ticketEntry.getText(), ticketEntry.getPrio()), handle -> {
					try {
						if (!handle.succeeded())
							err[0] = handle.cause();
					} finally {
						latch.countDown();
					}
				}
			);
		await(latch); // since this method implements a blocking api we have to wait
		if (err[0] != null)
			throw new RuntimeException("error", err[0]);
	}

	@Override
	public boolean moveTicketUp(String id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean moveTicketDown(String id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int removeTicketEntry(String id) {
		// TODO Auto-generated method stub
		return 0;
	}

	private TicketEntry rowmap(Row row) {
		final TicketEntry te = new TicketEntry();
		te.setTicket(row.getString("ticket"));
		te.setText(row.getString("text"));
		te.setPrio(row.getInteger("prio"));
		return te;
	}
}
