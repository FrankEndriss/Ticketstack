### Reactive
* Postgres driver [Github Link](https://github.com/reactiverse/reactive-pg-client) + [Doku](https://reactiverse.io/reactive-pg-client/guide/groovy/index.html)
* R2DBC [Announcement](https://spring.io/blog/2018/09/27/the-reactive-revolution-at-springone-platform-2018-part-1-n)
* Micronaut + Postgres [Doku](https://docs.micronaut.io/latest/guide/index.html#postgresSupport)

[Zurück auf Anfang](JPA.md)
