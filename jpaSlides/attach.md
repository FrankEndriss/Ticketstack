### attach/detach
* Entity-Objekte haben einen "externen Status"
* EntityManager per Session vs EntityManager per Request
* -per Session skaliert nicht
* -per Request erfordert ständiges attach
* [How to attach](https://stackoverflow.com/questions/912659/what-is-the-proper-way-to-re-attach-detached-objects-in-hibernate)

[Alternativen](alternativen.md)

