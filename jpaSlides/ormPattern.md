### ORM (Anti)-Pattern
* @Entity, Data classes oder "echte" Entitäten?
* [ORM Antipattern Erklärung yegor256](https://www.yegor256.com/2014/12/01/orm-offensive-anti-pattern.html)
* - stattdessen SQL-speaking-objects
* - Serialisierung soll/muss in der Klasse gekapselt sein, nicht public, nicht getrennt

[Hibernate/Caching](hibernate.md)

