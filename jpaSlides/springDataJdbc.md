### Spring Data JDBC
* einfache Version von JPA [Overview](https://spring.io/projects/spring-data-jdbc)
* kein Cache
* kein Lazy loading, Aggregates werden deshalb komplett geladen
* Update immer explizit, und Aggregates komplett
* da Updates immer explizit, kein attach/detach
* Events (ohne Einschränkungen)
* kleines [Beispiel](../app_v4_spring_data_jdbc/src/main/java/com/happypeople/ticketstack/app_v4_spring_data_jdbc/TicketEntryRepository.java)

[Speedment](speedment.md)
