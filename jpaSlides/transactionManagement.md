### Transaction Management
* Global Transaction vs Local Transaction [wtf](https://stackoverflow.com/questions/22221741/spring-global-transaction-vs-local-transaction)
* Transaction per EntityManager vs Transaction per Thread
* kein "auto transaction mode" wie in JDBC
* Pattern "Transaction per Web-Request"
* Spring TransactionTemplate, [Doku](https://docs.spring.io/spring/docs/5.1.2.RELEASE/spring-framework-reference/data-access.html#transaction-programmatic)
* @Transactional [Spring Doku](https://docs.spring.io/spring/docs/5.1.2.RELEASE/spring-framework-reference/data-access.html#transaction-declarative-annotations)
 * -Transaktion-Markierung ist nicht auf der Aufruferseite
 * -funktioniert nicht immer wie erwartet, zB weil AOP

[ORM Pattern](ormPattern.md)

