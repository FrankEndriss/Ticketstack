### No-SQL
* nicht relational, oft Dokument-basiert
* üblicherweise non blocking Schnittstellen verfügbar
* extra-Features, zB Firebase Auto-Update ohne reload
* zB Firebase Cloud Firestore [Promo](https://firebase.google.com/products/firestore/)

[JDBC-Template](jdbc.md)

