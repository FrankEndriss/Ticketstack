package com.happypeople.ticketstack.app_v3_spring_jpa_xml;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({ "com.happypeople.ticketstack" })
public class AppV3SpringJpaXmlApplication {

	public static void main(final String[] args) {
		SpringApplication.run(AppV3SpringJpaXmlApplication.class, args);
	}
}
