package com.happypeople.ticketstack.app_v3_spring_jpa_xml;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.happypeople.ticketstack.*;

public class TicketEntryListImpl implements TicketEntryList {
	private final static Logger log = LoggerFactory.getLogger(TicketEntryListImpl.class);

	@PersistenceContext
	private EntityManager em;

	public TicketEntryListImpl() {
		log.info("TicketEntryListImpl created");
	}

	@Override
	public List<TicketEntry> getAllTicketEntries() {
		// return em.createNativeQuery("SELECT * FROM tickets ORDER BY prio", TicketEntry.class).getResultList();
		return em.createQuery("SELECT t FROM TicketEntry t ORDER BY t.prio", TicketEntry.class).getResultList();
	}

	@Override
	@Transactional
	public void updateTicketText(final String ticket, final String updText) {
		final TicketEntry te=em.find(TicketEntry.class, ticket);
		if(te!=null) {
			te.setText(updText);
		}
	}

	@Override
	@Transactional
	public void insertTicket(final TicketEntry ticketEntry) {
		log.info("insertTicket: id="+ ticketEntry.getTicket() + " text="+ticketEntry.getText());

		em.createNativeQuery("INSERT INTO tickets (prio, text, ticket) SELECT COALESCE(min(prio), 1)-1, ?, ? FROM tickets")
		.setParameter(1, ticketEntry.getText())
		.setParameter(2, ticketEntry.getTicket())
		.executeUpdate();
	}

	@Override
	public TicketEntry getTicketEntry(final String ticket) {
		return em.find(TicketEntry.class, ticket);
	}

	@Override
	@Transactional
	public boolean moveTicketDown(final String ticket) {
		final TicketEntry thisTicket = getTicketEntry(ticket);
		if (thisTicket == null)
			return false;

		// select the ticket just after this ticket
		final List<TicketEntry> listAfter=em.createQuery("SELECT t FROM TicketEntry t WHERE t.prio > ?1 ORDER BY t.prio", TicketEntry.class)
				.setParameter(1, thisTicket.getPrio())
				.getResultList();

		if(listAfter.size()==0)
			return false;

		final TicketEntry ticketAfter = listAfter.get(0);

		final int thisPrio=thisTicket.getPrio();
		thisTicket.setPrio(ticketAfter.getPrio());
		ticketAfter.setPrio(thisPrio);

		return true;
	}

	@Override
	@Transactional
	public boolean moveTicketUp(final String ticket) {
		final TicketEntry thisTicket = getTicketEntry(ticket);
		if (thisTicket == null)
			return false;

		// select the ticket just before this ticket
		final List<TicketEntry> listBefore=em.createQuery("SELECT t FROM TicketEntry t WHERE t.prio < ?1 ORDER BY t.prio DESC", TicketEntry.class)
				.setParameter(1, thisTicket.getPrio())
				.getResultList();

		if(listBefore.size()==0)
			return false;

		final TicketEntry ticketBefore = listBefore.get(0);

		final int thisPrio=thisTicket.getPrio();
		thisTicket.setPrio(ticketBefore.getPrio());
		ticketBefore.setPrio(thisPrio);

		return true;
	}

	@Override
	@Transactional
	public int removeTicketEntry(final String ticket) {
		final TicketEntry te = getTicketEntry(ticket);
		if (te != null) {
			em.remove(te);
			return 1;
		}
		return 0;
	}
}
