package com.happypeople.ticketstack.app_v3_spring_jpa_xml;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.annotation.RequestScope;

import com.happypeople.ticketstack.*;

@Configuration
public class TicketEntryListBeanFactory {

	@Bean
	@RequestScope
	public TicketEntryList myDao() {
		return new TicketEntryListImpl();
	}
}
