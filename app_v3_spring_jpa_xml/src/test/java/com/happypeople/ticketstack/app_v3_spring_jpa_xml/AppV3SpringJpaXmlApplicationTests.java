package com.happypeople.ticketstack.app_v3_spring_jpa_xml;

import static org.junit.Assert.assertEquals;

import java.time.Duration;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;

import com.happypeople.ticketstack.TicketEntry;
import com.happypeople.ticketstack.TicketEntryList;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(initializers = {AppV3SpringJpaXmlApplicationTests.Initializer.class})
public class AppV3SpringJpaXmlApplicationTests {


	// see https://dzone.com/articles/testcontainers-and-spring-boot
	@ClassRule
	public static PostgreSQLContainer postgreSQLContainer =
	(PostgreSQLContainer) new PostgreSQLContainer("postgres:11")
	.withDatabaseName("ticketstack")
	.withUsername("postgres")
	.withPassword("postgres")
	.withStartupTimeout(Duration.ofSeconds(60));

	public static class Initializer
	implements ApplicationContextInitializer<ConfigurableApplicationContext> {
		@Override
		public void initialize(final ConfigurableApplicationContext configurableApplicationContext) {
			TestPropertyValues.of(
					"spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
					"spring.datasource.username=" + postgreSQLContainer.getUsername(),
					"spring.datasource.password=" + postgreSQLContainer.getPassword()
					).applyTo(configurableApplicationContext.getEnvironment());
		}

	}


	@Inject
	private TicketEntryList repo;

	private TicketEntry createTicketEntry(final String id, final String text, final int prio) {
		final TicketEntry te=new TicketEntry();
		te.setTicket(id);
		te.setText(text);
		te.setPrio(prio);
		return te;
	}
	@Before
	public void setUp() {
		for(final TicketEntry te : repo.getAllTicketEntries()) {
			repo.removeTicketEntry(te.getTicket());
		}
		repo.insertTicket(createTicketEntry(""+UUID.randomUUID(), "text"+UUID.randomUUID(), 1));
		repo.insertTicket(createTicketEntry(""+UUID.randomUUID(), "text"+UUID.randomUUID(), 2));
	}

	@Test
	public void contextLoads() {
		System.out.println(""+repo);
	}

	@Test
	public void testInserted() {
		final int count=repo.getAllTicketEntries().size();
		assertEquals("should be 2 since 2 tickets where inserted", 2, count);
	}

	@Test
	public void testInsert() {
		for(int i=0; i<100; i++) {
			repo.insertTicket(createTicketEntry(""+UUID.randomUUID(), "text"+UUID.randomUUID(), 0));
		}
	}

	@Test
	public void testMoveUp() {
		final List<TicketEntry> list=repo.getAllTicketEntries();
		final String id2=list.get(1).getTicket();
		repo.moveTicketUp(id2);

		final TicketEntry first=repo.getAllTicketEntries().get(0);
		assertEquals("second ticket should be first, now", id2, first.getTicket());
	}

	@Test
	public void testMoveDown() {
		final List<TicketEntry> list=repo.getAllTicketEntries();
		final String id1=list.get(0).getTicket();
		repo.moveTicketDown(id1);

		final TicketEntry second=repo.getAllTicketEntries().get(1);
		assertEquals("second ticket should be first, now", id1, second.getTicket());
	}

	@Test
	public void testMoveDownMulti() {
		repo.insertTicket(createTicketEntry(""+UUID.randomUUID(), "text"+UUID.randomUUID(), 0));

		final List<TicketEntry> listBeforeMove=repo.getAllTicketEntries();
		final String idFirst=listBeforeMove.get(0).getTicket();
		repo.moveTicketDown(idFirst);
		repo.moveTicketDown(idFirst);

		final List<TicketEntry> listAfterMove=repo.getAllTicketEntries();
		assertEquals("ticket should be last now", idFirst, listAfterMove.get(2).getTicket());
	}
}

