package com.happypeople.ticketstack.domainifreactorcore

import com.happypeople.ticketstack.TicketEntry

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/** Interface for TicketEntry persistence access, the Aggregate,
 * a list of TicketEntry.
 * This definition is the reactor.core based version.
 */
interface TicketEntryList {
	/**
	 * @return All TicketEntry in this list, sorted.
	 */
	fun getAllTicketEntries(): Mono<List<TicketEntry>>
	
	/**
	 * @return All TicketEntry in this list, sorted.
	 */
	fun streamTicketEntries() : Flux<TicketEntry>

	/** Updates the text of a ticket.
	 * @param id The id of the entry to update.
	 * @param updText The new text for that ticket.
	 */
	fun updateTicketText(id : String, updText : String) : Mono<Long>

	/** Inserts a new ticket into the list.
	 * @param ticketEntry A new ticket entry.
	 */
	fun insertTicket(ticketEntry : TicketEntry) : Mono<Long>

	/** Query the TicketEntry with id.
	 * @param id
	 * @return The ticket with the id id.
	 */
	fun getTicketEntry(id : String) : Mono<TicketEntry>

	/** Swaps the priorities of the ticket with id id, and the ticket just before that ticket
	 * (in a imaginary list where the tickets are sortet by prio).
	 * @param id
	 * @return true if a ticket was moved, else false.
	 */
	fun  moveTicketUp(id : String) : Mono<Boolean>;

	/** Swaps the priorities of the ticket with id id, and the ticket just after that ticket
	 * (in a imaginary list where the tickets are sortet by prio).
	 * @param id
	 * @return true if a ticket was moved, else false.
	 */
	fun moveTicketDown(id : String) : Mono<Boolean>

	/** Removes a TicketEntry.
	 * @param id
	 * @return Number of removed items, most likely 1 or 0 if notfound.
	 */
	fun removeTicketEntry(id : String) : Mono<Long>

}
