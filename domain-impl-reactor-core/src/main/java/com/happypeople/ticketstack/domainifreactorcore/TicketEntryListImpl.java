package com.happypeople.ticketstack.domainifreactorcore;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import com.happypeople.reactorcorewrappedjdbc.DefaultRxConnection;
import com.happypeople.reactorcorewrappedjdbc.RowMapper;
import com.happypeople.ticketstack.TicketEntry;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class TicketEntryListImpl implements TicketEntryList {
	private final DefaultRxConnection conn;

	final RowMapper<TicketEntry> rowMapper = (resultSet) -> {
		try {
			final TicketEntry te = new TicketEntry();
			te.setTicket(resultSet.getString("ticket"));
			te.setText(resultSet.getString("text"));
			te.setPrio(resultSet.getInt("prio"));
			return te;
		} catch (final SQLException e) {
			throw new RuntimeException(e);
		}
	};

	public TicketEntryListImpl(final DataSource dataSource) throws SQLException {
		this.conn = new DefaultRxConnection(dataSource.getConnection());
	}

	@Override
	public Mono<List<TicketEntry>> getAllTicketEntries() {
		return streamTicketEntries().collectList();
	}

	@Override
	public Flux<TicketEntry> streamTicketEntries() {
		return conn.executeQuery("SELECT * FROM tickets ORDER BY prio", rowMapper);
	}

	@Override
	public Mono<TicketEntry> getTicketEntry(final String id) {
		return conn.executeQuery("SELECT * FROM tickets WHERE ticket = ?", rowMapper, id).next();
	}

	@Override
	public Mono<Long> insertTicket(final TicketEntry te) {
		return conn.executeUpdate(
				"INSERT INTO tickets (ticket, text, prio) VALUES (SELECT ?, ?, COALESCE(min(prio), 1)-1 FROM tickets)",
				te.getTicket(), te.getText());
	}

	final static String sqlFindNextTicket = "SELECT * FROM tickets WHERE prio > ? ORDER BY prio LIMIT 1";
	final static String sqlFindPrevTicket = "SELECT * FROM tickets WHERE prio < ? ORDER BY prio DESC LIMIT 1";

	private Mono<TicketEntry> findTicketByPrioSelect(final String sql, final int prio) {
		return conn.executeQuery(sql, rowMapper, prio).next();
	}

	@Override
	public Mono<Boolean> moveTicketDown(final String ticketID) {
		return moveTicket(ticketID, sqlFindNextTicket);
	}

	@Override
	public Mono<Boolean> moveTicketUp(final String ticketID) {
		return moveTicket(ticketID, sqlFindPrevTicket);
	}

	private Mono<Boolean> moveTicket(final String ticketID, final String sqlFindOtherTicket) {
		final String sqlUpdate = "UPDATE tickets SET prio=? WHERE ticket=?";

		try {
			return conn.transactionStart().flatMap(l -> getTicketEntry(ticketID))
					.flatMap(ticket -> Mono
							.zip(Mono.just(ticket), findTicketByPrioSelect(sqlFindOtherTicket, ticket.getPrio()))
							.flatMap(tuple -> {
								return conn.executeUpdate(sqlUpdate, tuple.getT1().getPrio(), tuple.getT2().getTicket())
										.flatMap(l -> conn.executeUpdate(sqlUpdate, tuple.getT2().getPrio(),
												tuple.getT1().getTicket()))
										.map(l -> true);
							}));
		} catch (final Throwable t) {
			return conn.transactionRollback().flatMap(l -> Mono.error(t));
		}
	}

	@Override
	public Mono<Long> removeTicketEntry(final String ticket) {
		return conn.executeUpdate("DELETE FROM tickets WHERE ticket = ?", ticket);
	}

	@Override
	public Mono<Long> updateTicketText(final String ticket, final String text) {
		return conn.executeUpdate("UPDATE tickets SET text = ? WHERE ticket = ?", text, ticket);
	}

}