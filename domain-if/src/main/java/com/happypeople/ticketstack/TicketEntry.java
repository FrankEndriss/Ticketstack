package com.happypeople.ticketstack;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement // this annotation is needed by spring boot 1.x, TODO remove
public class TicketEntry {
	// TODO use Lombok
	
	public TicketEntry() {
		// jaxb needs this
	}

	private String ticket;
	private String text;
	private int prio;
	
	public String getTicket() {
		return ticket;
	}

	public void setTicket(final String ticket) {
		this.ticket = ticket;
	}

	public String getText() {
		return text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	public int getPrio() {
		return prio;
	}

	public void setPrio(final int prio) {
		this.prio = prio;
	}
}
