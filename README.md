Ticketstack README

Ticketstack is a simple list of links. Use it to sort the tickets you are currently working on.
* move entries up and down in the list
* edit them
* add new ones
* and remove them.

[Installation](https://gitlab.com/FrankEndriss/Ticketstack/wikis/Installation)

http://192.168.99.100:8087
http://192.168.99.100:8087/vaadin
http://192.168.99.100:8087/angular/index.html

