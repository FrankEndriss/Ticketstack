package com.happypeople.ticketstack.rxjava2;

import java.util.ArrayList;
import java.util.List;

import com.happypeople.ticketstack.TicketEntry;

import io.reactiverse.pgclient.PgClient;
import io.reactiverse.pgclient.PgPool;
import io.reactiverse.pgclient.PgPoolOptions;
import io.reactiverse.pgclient.Row;
import io.reactivex.Observable;
import io.reactivex.subjects.ReplaySubject;
import io.reactivex.subjects.Subject;

/** Implementierung des RxJava2 basierten Interfaces unter 
 * Nutzung von reactiverse.pgclient.
 */
public class TicketEntryListImpl implements TicketEntryList {
	
	private PgPool pgPool;

	/**
	 * Quick 'n dirty configuration, should be injected
	 * 
	 * @return The reactive database connection factory, like a jdbc driver
	 */
	public static PgPool init() {
		PgPoolOptions options = new PgPoolOptions().setPort(5432).setHost("postgres").setDatabase("ticketstack")
				.setUser("postgres").setPassword("postgres").setMaxSize(3);

		// Create the client pool
		return PgClient.pool(options);
	}

	TicketEntryListImpl(final PgPool pgPool) {
		this.pgPool = pgPool;
	}

	@Override
	public Observable<List<TicketEntry>> getAllTicketEntries() {
		final Subject<List<TicketEntry>> subject=ReplaySubject.create();

		pgPool.query("SELECT * FROM tickets ORDER BY prio", handle -> {
			// In which Thread is this method exexcuted???
			// Its not the caller, not a RxJava scheduler...
			// Seems to be the "Vertx" Thread.
			
			// Möglicherweise wäre es auch einfacher einen selbst implementierten
			// wrapper um JDBC zu nutzen, welcher in dem/den RxJava schedulern
			// aufgerufen wird.

			final List<TicketEntry> result=new ArrayList<TicketEntry>();
			try {
				if (handle.succeeded()) {
					handle.result().forEach(row -> {
						result.add(rowmap(row));
					});
					subject.onNext(result);
				} else {
					subject.onError(handle.cause());
				}
			} finally {
				subject.onComplete();
			}
		});	

		return subject;
	}

	@Override
	public Observable<Void> updateTicketText(String id, String updText) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Observable<Void> insertTicket(TicketEntry ticketEntry) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Observable<TicketEntry> getTicketEntry(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Observable<Boolean> moveTicketUp(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Observable<Boolean> moveTicketDown(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Observable<Integer> removeTicketEntry(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	private TicketEntry rowmap(final Row row) {
		final TicketEntry te = new TicketEntry();
		te.setTicket(row.getString("ticket"));
		te.setText(row.getString("text"));
		te.setPrio(row.getInteger("prio"));
		return te;
	}
}
