package com.happypeople.ticketstack.rxjava2;

import java.util.List;
import com.happypeople.ticketstack.TicketEntry;

import io.reactivex.Observable;

/** Interface for TicketEntry persistence access, the Aggregate, 
 * a list of TicketEntry.
 * This definition is the RxJava2 based version.
 */
public interface TicketEntryList {

	/**
	 * @return All TicketEntry in this list, sorted.
	 */
	Observable<List<TicketEntry>> getAllTicketEntries();

	/** Updates the text of a ticket.
	 * @param id The id of the entry to update.
	 * @param updText The new text for that ticket.
	 */
	Observable<Void> updateTicketText(String id, String updText);

	/** Inserts a new ticket into the list.
	 * @param ticketEntry A new ticket entry.
	 */
	Observable<Void> insertTicket(TicketEntry ticketEntry);

	/** Query the TicketEntry with id.
	 * @param id
	 * @return The ticket with the id id.
	 */
	Observable<TicketEntry> getTicketEntry(String id);

	/** Swaps the priorities of the ticket with id id, and the ticket just before that ticket 
	 * (in a imaginary list where the tickets are sortet by prio).
	 * @param id
	 * @return true if a ticket was moved, else false.
	 */
	Observable<Boolean>  moveTicketUp(String id);

	/** Swaps the priorities of the ticket with id id, and the ticket just after that ticket 
	 * (in a imaginary list where the tickets are sortet by prio).
	 * @param id
	 * @return true if a ticket was moved, else false.
	 */
	Observable<Boolean> moveTicketDown(String id);

	/** Removes a TicketEntry.
	 * @param id
	 * @return Number of removed items, most likely 1 or 0 if notfound.
	 */
	Observable<Integer> removeTicketEntry(String id);

}
