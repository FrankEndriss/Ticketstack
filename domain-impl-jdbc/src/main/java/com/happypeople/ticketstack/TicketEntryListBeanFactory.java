package com.happypeople.ticketstack;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TicketEntryListBeanFactory {

	@Inject
	private DataSource dataSource;

	@Bean
	public TicketEntryList myDao() {
		return new TicketEntryListImpl(dataSource);
	}
}
