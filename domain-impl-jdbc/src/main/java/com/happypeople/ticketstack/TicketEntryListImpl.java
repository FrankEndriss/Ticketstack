package com.happypeople.ticketstack;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
// import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Transactional;

public class TicketEntryListImpl implements TicketEntryList {
	private final static Logger log = LoggerFactory.getLogger(TicketEntryListImpl.class);

	private final DataSource dataSource;

	private final JdbcTemplate jdbcTemplate;
	private final TicketEntryRowMapper teRowMapper = new TicketEntryRowMapper();

	@Inject
	public TicketEntryListImpl(final DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(this.dataSource);
		log.info("TicketEntryListImpl created");
	}

	@Override
	public List<TicketEntry> getAllTicketEntries() {
		return jdbcTemplate.query("select * from tickets order by prio", teRowMapper);
	}

	@Override
	@Transactional
	public void updateTicketText(final String ticket, final String updText) {
		jdbcTemplate.update("update tickets set text = ? where ticket = ?", updText, ticket);
	}

	@Override
	@Transactional
	public void insertTicket(final TicketEntry ticketEntry) {
		log.info("insertTicket: id="+ ticketEntry.getTicket() + " text="+ticketEntry.getText());
		jdbcTemplate.update(
				// NOTE if the table is empty then min(prio) is null
				"INSERT INTO tickets (prio, text, ticket) SELECT COALESCE(min(prio), 1)-1, ?, ? FROM tickets",
				ticketEntry.getText(), ticketEntry.getTicket());
	}

	@Override
	public TicketEntry getTicketEntry(final String ticket) {
		try {
			return jdbcTemplate.queryForObject("select prio, text, ticket from tickets where ticket = ?",
					new Object[] { ticket }, teRowMapper);
		} catch (final Exception e) {
			// notfound
			return null;
		}
	}

	private final static class TicketEntryRowMapper implements RowMapper<TicketEntry> {
		@Override
		public TicketEntry mapRow(final ResultSet rs, final int rowNum) throws SQLException {
			final TicketEntry te = new TicketEntry();
			te.setPrio(rs.getInt("prio"));
			te.setText(rs.getString("text"));
			te.setTicket(rs.getString("ticket"));
			return te;
		}
	}

	@Override
	@Transactional
	public boolean moveTicketDown(final String ticket) {
		final TicketEntry thisTicket = getTicketEntry(ticket);
		if (thisTicket == null)
			return false;
		// select the ticket just after this ticket
		final TicketEntry ticketAfter = jdbcTemplate.queryForObject(
				"select * from tickets where prio > ? order by prio limit 1", new Object[] { thisTicket.getPrio() },
				teRowMapper);
		if (ticketAfter == null)
			return false;

		swapPrios(thisTicket, ticketAfter);
		return true;
	}

	@Override
	@Transactional
	public boolean moveTicketUp(final String ticket) {
		final TicketEntry thisTicket = getTicketEntry(ticket);
		if (thisTicket == null)
			return false;
		// select the ticket just before this ticket
		final TicketEntry ticketBefore = jdbcTemplate.queryForObject(
				"select * from tickets where prio < ? order by prio desc limit 1",
				new Object[] { thisTicket.getPrio() }, teRowMapper);
		if (ticketBefore == null)
			return false;

		swapPrios(thisTicket, ticketBefore);
		return true;
	}

	private void swapPrios(final TicketEntry first, final TicketEntry second) {
		updatePrio(second.getTicket(), first.getPrio());
		updatePrio(first.getTicket(), second.getPrio());
	}

	private void updatePrio(final String ticket, final int prio) {
		jdbcTemplate.update("update tickets set prio=? where ticket= ?", prio, ticket);
	}

	@Override
	public int removeTicketEntry(final String ticket) {
		return jdbcTemplate.update("delete from tickets where ticket=?", ticket);
	}

}
