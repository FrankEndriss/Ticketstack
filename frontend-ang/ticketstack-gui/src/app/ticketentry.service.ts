
import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable ,  Subject, BehaviorSubject } from 'rxjs';
import { tap, flatMap, map } from 'rxjs/operators';

import { TicketEntry } from './domain/TicketEntry';

@Injectable()
export class TicketentryService {
  private apiPrefix = 'http://localhost:8087/api/';

  private ticketlistSubject: Subject<TicketEntry[]> = new BehaviorSubject<TicketEntry[]>([]);

  constructor(private http: HttpClient) {
  }

  /** @return Observable which emits the current list of TicketEntry on any change of that list.
   */
  getTicketlistEmitter(): Observable<TicketEntry[]> {
    // whenever some component registers as listener is a good time to reload the data
    this.triggerReloadTicketEntryList();
    return this.ticketlistSubject.asObservable();
  }

  /**
   * Triggers reload of the ticket list.
   * @return Optional Observable which can be used to observe the
   * completion of this reload operation.
   * However, the reload is started independent of subcription to the returned Observable.
   */
  private triggerReloadTicketEntryList(): Observable<void> {
    const ret: Subject<void> = new Subject();

    console.log('triggerReloadTicketEntryList()');

    this.http.get<TicketEntry[]>(this.apiPrefix)
      .subscribe(
        list => this.ticketlistSubject.next(this.markFirstAndLast(list)),
        err => this.ticketlistSubject.error(err),
        () => ret.complete()
      );

    return ret.asObservable();
  }

  private markFirstAndLast(arr: TicketEntry[]): TicketEntry[] {
    if (arr.length > 0) {
      arr[0]['first'] = true;
      arr[arr.length - 1]['last'] = true;
    }
    return arr;
  }

  private postOnEntry(entry: TicketEntry, pathPostfix: string) {
    const url = this.apiPrefix + entry.ticket + pathPostfix;
    const ret: Subject<void> = new Subject();

    this.http.post<void>(url, '', {})
      .pipe(flatMap(() => this.triggerReloadTicketEntryList()))
      .subscribe(null,
        (err) => this.ticketlistSubject.error(err),
        () => ret.complete()
     );

     return ret.asObservable();
  }

  public submit(ticket: string, text: string): Observable<void> {
    console.log('submit: ', ticket, text);
    const url = this.apiPrefix;
    const ret: Subject<void> = new Subject();

    const ticketEntry: TicketEntry = {
      ticket: ticket,
      text: text,
    };

    this.http.post<void>(url, ticketEntry, {})
      .pipe(flatMap(() => this.triggerReloadTicketEntryList()))
      .subscribe(null,
        (err) => this.ticketlistSubject.error(err),
        () => ret.complete()
     );

     return ret.asObservable();
  }

  public delete(entry: TicketEntry): Observable<void> {
    return this.postOnEntry(entry, '/delete');
  }

  public moveUp(entry: TicketEntry): Observable<void> {
    return this.postOnEntry(entry, '/up');
  }

  public moveDown(entry: TicketEntry): Observable<void> {
    return this.postOnEntry(entry, '/down');
  }
}


