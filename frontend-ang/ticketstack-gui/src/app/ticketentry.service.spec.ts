
import { TestBed, inject } from '@angular/core/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';

import { TicketentryService } from './ticketentry.service';

describe('TicketentryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TicketentryService, HttpClient, HttpHandler]
    });
  });

  it('should be created', inject([TicketentryService], (service: TicketentryService) => {
    expect(service).toBeTruthy();
  }));
});
