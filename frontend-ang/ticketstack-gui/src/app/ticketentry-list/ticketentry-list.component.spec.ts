import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketentryListComponent } from './ticketentry-list.component';
import { TicketentryService } from '../ticketentry.service';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('TicketentryListComponent', () => {
  let component: TicketentryListComponent;
  let fixture: ComponentFixture<TicketentryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketentryListComponent ],
      providers: [ TicketentryService, HttpClient, HttpHandler]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketentryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
