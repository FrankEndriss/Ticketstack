
import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { TicketentryService } from '../ticketentry.service';
import { TicketEntry } from '../domain/TicketEntry';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-ticketentry-list',
  templateUrl: './ticketentry-list.component.html',
  styleUrls: ['./ticketentry-list.component.css']
})
export class TicketentryListComponent implements OnInit {

  tickets$: Observable<TicketEntry[]>;
  busy: Subscription;

  displayedColumns = [ 'btnUp', 'btnDown', 'ticket', 'text', 'btnDelete' ];

  constructor(private ticketsService: TicketentryService) { }

  theForm = new FormGroup( {
    ticket: new FormControl(),
    text: new FormControl()
  });

  ngOnInit() {
    this.tickets$ = this.ticketsService.getTicketlistEmitter();
  }

  public entryClicked(entry: TicketEntry) {
    console.log('entryClicked', entry);
    this.theForm.controls['ticket'].setValue(entry.ticket);
    this.theForm.controls['text'].setValue(entry.text);
  }

  public submit() {
    this.ticketsService.submit(this.theForm.value.ticket, this.theForm.value.text);
  }

  public delete(entry: TicketEntry) {
    this.busy = this.ticketsService.delete(entry).subscribe();
  }

  public moveUp(entry: TicketEntry) {
    this.busy = this.ticketsService.moveUp(entry).subscribe();
  }

  public moveDown(entry: TicketEntry) {
    this.busy = this.ticketsService.moveDown(entry).subscribe();
  }
}
