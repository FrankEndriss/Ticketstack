package net.neobp.ticketstack;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.happypeople.ticketstack.TicketEntryList;
import com.happypeople.ticketstack.TicketEntryListImpl;

@Configuration
public class TicketEntryDaoFactory {

	@Inject
	private DataSource dataSource;
	
	@Bean
	public TicketEntryList myDao() {
		return new TicketEntryListImpl(dataSource);
	}
}
