-- Recreate index on tickets.prio to be not unique
DROP INDEX tickets_prio_idx;

CREATE INDEX tickets_prio_idx
  ON tickets (prio);

