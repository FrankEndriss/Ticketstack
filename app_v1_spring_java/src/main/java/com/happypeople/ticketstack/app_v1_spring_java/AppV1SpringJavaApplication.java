package com.happypeople.ticketstack.app_v1_spring_java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({ "com.happypeople.ticketstack" })
public class AppV1SpringJavaApplication {

	public static void main(final String[] args) {
		SpringApplication.run(AppV1SpringJavaApplication.class, args);
	}
}
