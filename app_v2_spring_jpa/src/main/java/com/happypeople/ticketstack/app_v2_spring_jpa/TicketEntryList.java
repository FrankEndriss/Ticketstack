package com.happypeople.ticketstack.app_v2_spring_jpa;

import java.util.List;

/** Interface for TicketEntry persistence access, the Aggregate,
 * a list of TicketEntry.
 */
public interface TicketEntryList {

	/**
	 * @return All TicketEntry in this list, sorted by prio.
	 */
	List<TicketEntry> getAllTicketEntries();

	/** Updates the text of a ticket.
	 * @param id The id of the entry to update.
	 * @param updText The new text for that ticket.
	 */
	void updateTicketText(String id, String updText);

	/** Inserts a new ticket into the list.
	 * @param ticketEntry A new ticket entry.
	 */
	void insertTicket(TicketEntry ticketEntry);

	/** Query the TicketEntry with id.
	 * @param id
	 * @return The TicketEntry with the ticket-id id.
	 */
	TicketEntry getTicketEntry(String id);

	/** Swaps the priorities of the ticket with id id, and the ticket just before that ticket
	 * (tickets are sortet by prio).
	 * @param id
	 * @return true if a ticket was moved, else false.
	 */
	boolean moveTicketUp(String id);

	/** Swaps the priorities of the ticket with id id, and the ticket just after that ticket
	 * (tickets are sortet by prio).
	 * @param id
	 * @return true if a ticket was moved, else false.
	 */
	boolean moveTicketDown(String id);

	/** Removes a TicketEntry.
	 * @param id
	 * @return Number of removed items, most likely 1 or 0 if notfound.
	 */
	int removeTicketEntry(String id);

}