package com.happypeople.ticketstack.app_v2_spring_jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({ "com.happypeople" })
public class AppV2SpringJpaApplication {

	public static void main(final String[] args) {
		SpringApplication.run(AppV2SpringJpaApplication.class, args);
	}
}
