package com.happypeople.ticketstack.app_v2_spring_jpa;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.annotation.RequestScope;

@Configuration
public class TicketEntryListBeanFactory {

	@Bean
	@RequestScope
	public TicketEntryList myDao() {
		return new TicketEntryListImpl();
	}
}
