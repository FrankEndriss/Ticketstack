
class TicketstackTable extends HTMLElement {
    constructor() {
        super();
        this.tableData='loading table data...';

        this.myStyle=`
            <style>
                tr.even {
                    background: lightgrey;
                }
                tr.odd {
                    background: grey;
                }
                td div {
                    padding: 16px;
                }
            </style>
        `;

    }

    connectedCallback() {
        this.reloadFromServer()
    }

    reloadFromServer() {
        fetch("http://localhost:8087/api/").then(resp => resp.json(), this.someerr ).then(arr => {
            console.log('fetched from server: ', arr);
            var i=0;
            var myHtml='<table><th>Up</th><th>Down</th><th>Ticket</th><th>Text</th><th>Delete</th>'
            var even=false
            var first=true
            var size=arr.length
            var i=1
            for(var row of arr) {
                row.ticket='<a href="http://bla.laber">'+row.ticket+'<a>';
                myHtml=myHtml+`
                <tr class=${even ? 'even' : 'odd'}>
                <td><div>${i>1 ? '<button>Up</button>' : ''}</div></td>
                <td><div>${i<size ? '<button>Down</button>' : ''}</div></td>
                <td><div>${row.ticket}</div></td>
                <td><div>${row.text}</div></td>
                <td><div><button>Delete</button></div></td>
                </tr>
                `
                even=!even;
                i=i+1
            }
            var myHtml=myHtml+"</table>"
            this.innerHTML=this.myStyle+myHtml;
        });
    }

    setMyTableData(arg) {
        this.tableData=arg
    }

    someerr(err) {
        console.log(err)
        alert(err)
    }
}

class TicketstackForm extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        /* Build a nested grid layout. */
        this.innerHTML=`
        <style>
            thisform {
                display: grid;

                grid-template-columns: 1fr 3fr;
                grid-template-rows: 2fr 1fr;
                grid-template-areas: 
                    "linksoben rechts"
                    "linksunten rechts"
                    ;
            }

            linksoben {
                grid-area: linksoben;
                background: green;
            }

            linksunten {
                grid-area: linksunten;
                background: lightblue;
            }

            rechts {
                grid-area: rechts;
                background: lightgreen;
            }
        </style>

        <thisform>
            <linksunten>linksunten</linksunten>
            <linksoben>linksoben</linksoben>
            <rechts>text ... rechts</rechts>
        </thisform>

        `;
    }

}

customElements.define('my-table', TicketstackTable);
customElements.define('my-form', TicketstackForm);
