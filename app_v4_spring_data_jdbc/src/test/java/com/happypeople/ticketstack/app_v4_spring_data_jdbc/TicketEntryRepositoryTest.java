package com.happypeople.ticketstack.app_v4_spring_data_jdbc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.time.Duration;
import java.util.UUID;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;

@RunWith(SpringRunner.class)
@DataJdbcTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
@ContextConfiguration(initializers = {TicketEntryRepositoryTest.Initializer.class})
public class TicketEntryRepositoryTest {

	// see https://dzone.com/articles/testcontainers-and-spring-boot
	@ClassRule
	public static PostgreSQLContainer postgreSQLContainer =
	(PostgreSQLContainer) new PostgreSQLContainer("postgres:11")
	.withDatabaseName("ticketstack")
	.withUsername("postgres")
	.withPassword("postgres")
	.withStartupTimeout(Duration.ofSeconds(60));

	public static class Initializer
	implements ApplicationContextInitializer<ConfigurableApplicationContext> {
		@Override
		public void initialize(final ConfigurableApplicationContext configurableApplicationContext) {
			TestPropertyValues.of(
					"spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
					"spring.datasource.username=" + postgreSQLContainer.getUsername(),
					"spring.datasource.password=" + postgreSQLContainer.getPassword()
					).applyTo(configurableApplicationContext.getEnvironment());
		}
	}

	@Autowired
	TicketEntryRepository repo;

	@Before
	public void initEach() {
		repo.deleteAll();
		repo.create("Ticket-1", "text"+UUID.randomUUID() );
		repo.create("Ticket-2", "text"+UUID.randomUUID() );
	}

	@Test
	public void testConfig() {
		assertEquals("init should create two rows", 2L, repo.count());
	}

	@Test
	public void simpleUpdate() {
		final String newText="some new text";
		final TicketEntry te=repo.findById("Ticket-1").get();
		te.text=newText;
		repo.save(te);
		final TicketEntry teUpdated=repo.findById("Ticket-1").get();
		assertNotEquals("shouldn't be same", te, teUpdated);
		assertEquals("same text", newText, teUpdated.text);
	}

}
