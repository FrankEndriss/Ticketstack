package com.happypeople.ticketstack.app_v4_spring_data_jdbc;

import java.time.Duration;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(initializers = {AppV4SpringDataJdbcApplicationTests.Initializer.class})
public class AppV4SpringDataJdbcApplicationTests {
	// see https://dzone.com/articles/testcontainers-and-spring-boot
	@ClassRule
	public static PostgreSQLContainer postgreSQLContainer =
	(PostgreSQLContainer) new PostgreSQLContainer("postgres:11")
	.withDatabaseName("ticketstack")
	.withUsername("postgres")
	.withPassword("postgres")
	.withStartupTimeout(Duration.ofSeconds(60));

	public static class Initializer
	implements ApplicationContextInitializer<ConfigurableApplicationContext> {
		@Override
		public void initialize(final ConfigurableApplicationContext configurableApplicationContext) {
			TestPropertyValues.of(
					"spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
					"spring.datasource.username=" + postgreSQLContainer.getUsername(),
					"spring.datasource.password=" + postgreSQLContainer.getPassword()
					).applyTo(configurableApplicationContext.getEnvironment());
		}
	}

	@Test
	public void contextLoads() {
	}

}
