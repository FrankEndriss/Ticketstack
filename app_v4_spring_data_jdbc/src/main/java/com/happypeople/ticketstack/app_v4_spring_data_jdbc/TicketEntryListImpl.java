package com.happypeople.ticketstack.app_v4_spring_data_jdbc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class TicketEntryListImpl implements TicketEntryList {
	private final static Logger log = LoggerFactory.getLogger(TicketEntryListImpl.class);

	@Autowired
	private TicketEntryRepository repo;

	public TicketEntryListImpl() {
		log.info("TicketEntryListImpl created");
	}

	private <T> List<T> makeList(final Iterable<T> iterable) {
		final List<T> list=new ArrayList<T>();
		iterable.forEach(list::add);
		return list;
	}

	private List<TicketEntry> sortByPrio(final List<TicketEntry> list) {
		Collections.sort(list, (te1, te2) -> te1.prio-te2.prio);
		return list;
	}

	@Override
	public List<TicketEntry> getAllTicketEntries() {
		return sortByPrio(makeList(repo.findAll()));
		// return makeList(repo.findAll(Sort.by("prio")));
	}

	@Override
	// @Transactional
	public void updateTicketText(final String ticket, final String updText) {
		final Optional<TicketEntry> teOpt=repo.findById(ticket);
		if(teOpt.isPresent()) {
			final TicketEntry te=teOpt.get();
			te.text=updText;
			repo.save(te);
		}
	}

	@Override
	// @Transactional
	public void insertTicket(final TicketEntry ticketEntry) {
		log.info("insertTicket: id="+ ticketEntry.ticket + " text="+ticketEntry.text);
		repo.create(ticketEntry.ticket, ticketEntry.text);
	}

	@Override
	public TicketEntry getTicketEntry(final String ticket) {
		return repo.findById(ticket).orElse(null);
	}

	@Override
	@Transactional
	public boolean moveTicketDown(final String ticket) {
		final TicketEntry thisTicket = getTicketEntry(ticket);
		if (thisTicket == null)
			return false;

		// select the ticket just after this ticket
		final Iterator<TicketEntry> iter = repo.ticketsAfter(thisTicket.prio).iterator();
		if(!iter.hasNext())
			return false;

		final TicketEntry ticketAfter = iter.next();
		final int thisPrio=thisTicket.prio;
		thisTicket.prio=ticketAfter.prio;
		ticketAfter.prio=thisPrio;
		repo.save(thisTicket);
		repo.save(ticketAfter);
		return true;
	}

	@Override
	@Transactional
	public boolean moveTicketUp(final String ticket) {
		final TicketEntry thisTicket = getTicketEntry(ticket);
		if (thisTicket == null)
			return false;

		// select the tickets just before this ticket
		final Iterator<TicketEntry> iter = repo.ticketsBeforeDesc(thisTicket.prio).iterator();
		if(!iter.hasNext())
			return false;

		final TicketEntry ticketBefore = iter.next();
		final int thisPrio=thisTicket.prio;
		thisTicket.prio=ticketBefore.prio;
		ticketBefore.prio=thisPrio;
		repo.save(thisTicket);
		repo.save(ticketBefore);
		return true;
	}

	@Override
	@Transactional
	public int removeTicketEntry(final String ticket) {
		final TicketEntry te = getTicketEntry(ticket);
		if (te != null) {
			repo.delete(te);
			return 1;
		}
		return 0;
	}
}
