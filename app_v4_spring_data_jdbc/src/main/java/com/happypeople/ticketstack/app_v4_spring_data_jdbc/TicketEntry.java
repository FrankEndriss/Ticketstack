package com.happypeople.ticketstack.app_v4_spring_data_jdbc;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table(value="tickets")
public class TicketEntry {

	@Id
	String ticket;
	String text;
	int prio;

	public String getTicket() {
		return ticket;
	}

	public void setTicket(final String ticket) {
		this.ticket = ticket;
	}

	public String getText() {
		return text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	public int getPrio() {
		return prio;
	}

	public void setPrio(final int prio) {
		this.prio = prio;
	}
}
