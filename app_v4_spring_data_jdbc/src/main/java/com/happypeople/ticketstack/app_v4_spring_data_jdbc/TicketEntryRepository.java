package com.happypeople.ticketstack.app_v4_spring_data_jdbc;

import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

// public interface TicketEntryRepository extends CrudRepository<TicketEntry, String> {
public interface TicketEntryRepository extends PagingAndSortingRepository<TicketEntry, String> {

	@Modifying
	@Query("INSERT INTO tickets (prio, text, ticket) SELECT COALESCE(min(prio), 1)-1, :text, :ticket FROM tickets")
	boolean create(@Param("ticket") String ticket, @Param("text") String text);

	@Query("SELECT tickets.* FROM tickets WHERE prio > :prio ORDER BY prio")
	Iterable<TicketEntry> ticketsAfter(@Param("prio") int prio);

	@Query("SELECT tickets.* FROM tickets WHERE prio < :prio ORDER BY prio DESC")
	Iterable<TicketEntry> ticketsBeforeDesc(@Param("prio") int prio);
}
