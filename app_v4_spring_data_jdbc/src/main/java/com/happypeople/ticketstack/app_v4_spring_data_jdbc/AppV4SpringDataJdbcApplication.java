package com.happypeople.ticketstack.app_v4_spring_data_jdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({ "com.happypeople.ticketstack" })
public class AppV4SpringDataJdbcApplication {

	public static void main(final String[] args) {
		SpringApplication.run(AppV4SpringDataJdbcApplication.class, args);
	}
}
